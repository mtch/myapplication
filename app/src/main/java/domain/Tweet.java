package domain;

import java.io.Serializable;

/**
 * Created by suannamara on 17/10/2015.
 */
public class Tweet implements Serializable{

    private String text;
    private String date;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Tweet{" +
                "text='" + text + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}
