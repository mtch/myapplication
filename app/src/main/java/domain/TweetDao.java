package domain;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by suannamara on 17/10/2015.
 */
public class TweetDao {

    final private static String TWEETS_NAME = "TWEETS";

    public void saveTweet(Context context, Tweet tweet){

        List<Tweet> tweets = getTweets(context);


        Log.d("conorbonor", tweets.toString());

        tweets.add(tweet);

        SharedPreferences prefs= context.getSharedPreferences(context.getApplicationInfo().name, Context.MODE_PRIVATE);
        String tweetString = prefs.getString(TWEETS_NAME, "");
        Gson gson = new Gson();


        String newTweetString = gson.toJson(tweets);
        Log.d("conorbonor", newTweetString);

        prefs.edit().putString(TWEETS_NAME, newTweetString).apply();

      /*  JsonParser parser=new JsonParser();
//object arr example
        JsonArray arr=parser.parse(mPrefs.getString("myArrKey", null)).getAsJsonArray();
        events=new Event[arr.size()];
        int i=0;
        for (JsonElement jsonElement : arr)
            events[i++]=gson.fromJson(jsonElement, Event.class);
//Object example
        pagination=gson.fromJson(parser.parse(jsonPagination).getAsJsonObject(), Pagination.class);*/


    }


    public void clearTweets(Context context){
        SharedPreferences prefs= context.getSharedPreferences(context.getApplicationInfo().name, Context.MODE_PRIVATE);
        prefs.edit().putString(TWEETS_NAME, "").apply();
    }

    public List<Tweet> getTweets(Context context){

        SharedPreferences prefs= context.getSharedPreferences(context.getApplicationInfo().name, Context.MODE_PRIVATE);
        String tweetString = prefs.getString(TWEETS_NAME, "");

       // Log.d("conorbonor", tweetString);
        List<Tweet> tweets;

        Gson gson = new Gson();
        if(tweetString.equals("")){
            tweets = new ArrayList<>();
        }else {


            tweets = gson.fromJson(tweetString, new TypeToken<List<Tweet>>() {}.getType());
        }

        return tweets;

    }


}
