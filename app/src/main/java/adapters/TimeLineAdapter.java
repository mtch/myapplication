package adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.suannamara.myapplication.R;

import org.w3c.dom.Text;

import java.util.List;

import domain.Tweet;

public class TimeLineAdapter extends BaseAdapter {
    private Context context;
    private List<Tweet> tweets;

    public TimeLineAdapter(Context context, List<Tweet> tweets) {
        this.context = context;
        Log.d("conorbonor", "sdfsdf" + tweets + "");
        this.tweets = tweets;
    }

    @Override
    public int getCount() {
        return tweets.size();
    }

    @Override
    public Object getItem(int position) {
        return tweets.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {



        if(convertView == null) {
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.position = position;

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.item_tweet, parent, false);
            viewHolder.text = (TextView) convertView.findViewById(R.id.tweet_text);
            viewHolder.date = (TextView) convertView.findViewById(R.id.tweet_date);
            convertView.setTag(viewHolder);

        }

        ViewHolder viewHolder = (ViewHolder)convertView.getTag();
        Tweet tweet = tweets.get(position);
        viewHolder.text.setText(tweet.getText());
        viewHolder.date.setText(tweet.getDate());
        // change the icon for Windows and iPhone


        return convertView;
    }


    private static class ViewHolder{
        TextView text;
        TextView date;
        int position;

    }

} 
