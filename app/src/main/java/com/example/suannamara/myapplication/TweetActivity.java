package com.example.suannamara.myapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import domain.Tweet;
import domain.TweetDao;

public class TweetActivity extends AppCompatActivity {

    final private static int TWEET_LENGTH = 140;

    private EditText tweetEditText;
    private TextView lettersLeftCounter;
    private Button sendTweetButton;
    private TextView dateText;

    private TweetDao tweetDao;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tweet);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tweetEditText = (EditText)findViewById(R.id.tweet_text);
        lettersLeftCounter = (TextView)findViewById(R.id.letters_left_counter);
        sendTweetButton = (Button)findViewById(R.id.send_tweet_button);
        dateText = (TextView)findViewById(R.id.date);

        this.tweetDao = new TweetDao();

        setUpEventHandlers();
        setDate();



    }


    private void setDate(){
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
        String formattedDate = df.format(c.getTime());
        dateText.setText(formattedDate);

    }


    private void onTweet(){
        Toast.makeText(TweetActivity.this, R.string.message_sent, Toast.LENGTH_LONG).show();
        Tweet tweet = new Tweet();
        tweet.setText(tweetEditText.getText() + "");
        tweet.setDate(dateText.getText() + "");

        tweetDao.saveTweet(this, tweet);

        /*SharedPreferences mPrefs= this.getSharedPreferences(this.getApplicationInfo().name, Context.MODE_PRIVATE);
        SharedPreferences.Editor ed=mPrefs.edit();
        Gson gson = new Gson();
        ed.putString("myObjectKey", gson.toJson(objectToSave));
        ed.commit();*/
    }

    private void setUpEventHandlers(){

        tweetEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                lettersLeftCounter.setText( (TWEET_LENGTH - tweetEditText.getText().toString().length()) + "" );
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        sendTweetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onTweet();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
       // getMenuInflater().inflate(R.menu.menu_timeline, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
