package com.example.suannamara.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

import adapters.TimeLineAdapter;
import domain.Tweet;
import domain.TweetDao;

public class TimeLineActivity extends AppCompatActivity {


    private TweetDao tweetDao;

    private ListView timeLineListView;
    private List<Tweet> tweets = new ArrayList<>();
    private TimeLineAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_line);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        tweetDao = new TweetDao();

        timeLineListView = (ListView)findViewById(R.id.time_line_list_view);
        adapter = new TimeLineAdapter(this, tweets);
        timeLineListView.setAdapter(adapter);



    }

    @Override
    public void onResume() {
        super.onResume();
        fetchTweets();


    }

    public void fetchTweets(){
        tweets.clear();
        tweets.addAll(tweetDao.getTweets(this));
        adapter.notifyDataSetChanged();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_timeline, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.action_tweet) {
            Intent intent = new Intent(this, TweetActivity.class);
            startActivity(intent);
            return true;
        }

        if (id == R.id.action_clear) {
            tweetDao.clearTweets(this);
            fetchTweets();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
